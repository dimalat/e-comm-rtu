import ServiceController from './controllers/service-controller.js'
import ServiceUseCase from '../../domain/use-cases/service.js'
import ServiceRepository from '../repositories/service-repository.js'
import multer from 'multer'
import express from 'express'
import serviceSchema from '../../domain/models/service-schema.js'
import categorySchema from '../../domain/models/category-schema.js'

const router = express.Router()

const categories = '/categories'
const register = '/services'
const listServices = '/list-services' //route for EJS template
const login = '/login' //route for EJS template

async function createServiceController() {
    return new ServiceController(
        new ServiceUseCase(
            await ServiceRepository.create(),
            serviceSchema.serviceSchema,
            categorySchema.categorySchema
        ));
}


router.post(register, multer().none(), async (req, res, next) => {
    (await createServiceController()).register(req, res)
        .catch(err => res.status(400).json(err))
})

router.get(categories, async (req, res, next) => {
    (await createServiceController()).getCategories(res)
        .catch(err => res.status(400).json(err))
})
router.post(categories, multer().none(), async (req, res, next) => {
    (await createServiceController()).addCategory(req,res)
        .catch(err => res.status(400).json(err))
})
/*
Login
*/
router.get(login, async (req, res, next) => {
    let service = await createServiceController();
    try{
        service.renderLogin(res, req.session)
    }
    catch(e){
        res.send(e);
    }
})

router.post(login, multer().none(), async (req, res, next) => {
    let service = await createServiceController();
    try{
        await service.checkCredAndRenderLogin(req, res, req.session)
    }
    catch(e){
        res.send(e);
    }
})

router.get(listServices, multer().none(), async (req, res, next) => {


    
    if(! req.session.name){
        req.session.cart = []
        req.session.name = Math.random(); 

    }else{

        req.session.cart.push(req.query.product)

    }

    (await createServiceController()).renderServices(res, req.session)
        .catch(err => res.status(400).json(err))
})



export default router

